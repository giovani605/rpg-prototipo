package com.example.rpg_prototipo;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.example.rpg_prototipo.adapters.ItemInventario;
import com.example.rpg_prototipo.adapters.ListaInvetarioAdapter;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TelaInventario#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TelaInventario extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public TelaInventario() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TelaInventario.
     */
    // TODO: Rename and change types and number of parameters
    public static TelaInventario newInstance(String param1, String param2) {
        TelaInventario fragment = new TelaInventario();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_tela_inventario, container, false);

        ArrayList<ItemInventario> listainventario = new ArrayList<>();
        ItemInventario itemInventario1 = new ItemInventario("Poçao Magica", "Super poçao legal salva vidas");
        listainventario.add(itemInventario1);
        ItemInventario itemInventario2 = new ItemInventario("Mapa", "O MAPA");
        listainventario.add(itemInventario2);
        ItemInventario itemInventario3 = new ItemInventario("Amuleto Sombrio", "O amuleto emite um energia sobria e fria");
        listainventario.add(itemInventario3);

        ListAdapter adapter = new ListaInvetarioAdapter(getActivity(),listainventario);

        ListView lista = (ListView) v.findViewById(R.id.frag_inventario_lista_view);

        lista.setAdapter(adapter);

        this.getActivity().setTitle("Inventario");


        return v;
    }

}
