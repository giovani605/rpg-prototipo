package com.example.rpg_prototipo;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.example.rpg_prototipo.adapters.ItemInventario;
import com.example.rpg_prototipo.adapters.ItemRegra;
import com.example.rpg_prototipo.adapters.ListaInvetarioAdapter;
import com.example.rpg_prototipo.adapters.ListaRegrasAdapter;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TelaRegras#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TelaRegras extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public TelaRegras() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TelaRegras.
     */
    // TODO: Rename and change types and number of parameters
    public static TelaRegras newInstance(String param1, String param2) {
        TelaRegras fragment = new TelaRegras();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_tela_regras, container, false);

        ArrayList<ItemRegra> listainventario = new ArrayList<>();
        ItemRegra itemInventario1 = new ItemRegra("Ataque", "Rode um d20 some as profecienas contra a defesa do inimigo, se seu numero for maior ou igual o ataque foi ums sucesso");
        listainventario.add(itemInventario1);
        ItemRegra itemInventario2 = new ItemRegra("Ataque", "Rode um d20 some as profecienas contra a defesa do inimigo, se seu numero for maior ou igual o ataque foi ums sucesso");
        listainventario.add(itemInventario2);
        ItemRegra itemInventario3 = new ItemRegra("Ataque", "Rode um d20 some as profecienas contra a defesa do inimigo, se seu numero for maior ou igual o ataque foi ums sucesso");
        listainventario.add(itemInventario3);

        ListAdapter adapter = new ListaRegrasAdapter(getActivity(),listainventario);

        ListView lista = (ListView) v.findViewById(R.id.frag_regras_lista_view);

        lista.setAdapter(adapter);

        this.getActivity().setTitle("Regras");


        return v;
    }

}
