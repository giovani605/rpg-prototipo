package com.example.rpg_prototipo;

import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements TelaAtalhos.OnFragmentInteractionListener, TelaPrincipal.OnFragmentInteractionListener {
    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.bottom_nav_view, new TelaPrincipal()).commit();

                    return true;
                case R.id.navigation_regras:
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.bottom_nav_view, new TelaRegras()).commit();
                    return true;
                case R.id.navigation_atalhos:
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.bottom_nav_view, new TelaAtalhos()).commit();
                    return true;
                case R.id.navigation_inventario:
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.bottom_nav_view, new TelaInventario()).commit();
                    return true;
                case R.id.navigation_Habilidades:
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.bottom_nav_view, new TelaHabilidades()).commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        mTextMessage = findViewById(R.id.message);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.bottom_nav_view, new TelaPrincipal()).commit();
        this.setTitle("Inicio");
    }

    @Override
    public void cliqueAtalho(View v){

        switch (v.getId()) {
            case R.id.frag_atalhos_botao_Equipamento:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.bottom_nav_view, new TelaPersonagem()).commit();


                return ;
            case R.id.frag_atalhos_botao_habilidades:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.bottom_nav_view, new TelaHabilidades()).commit();

                return ;
            case R.id.frag_atalhos_botao_Inventario:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.bottom_nav_view, new TelaInventario()).commit();

                return ;
            case R.id.frag_atalhos_botao_principal:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.bottom_nav_view, new TelaPrincipal()).commit();

                return ;
            case R.id.frag_atalhos_botao_partida:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.bottom_nav_view, new TelaPartida()).commit();

                return ;
            case R.id.frag_atalhos_botao_regras:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.bottom_nav_view, new TelaRegras()).commit();

                return ;

        }

    }


    @Override
    public void cliquePrincipal(View v){
         switch (v.getId()) {
             case R.id.frag_principal_botao_inventario:
                 getSupportFragmentManager().beginTransaction()
                         .replace(R.id.bottom_nav_view, new TelaInventario()).commit();

                 return;
             case R.id.frag_principal_botao_partida:
                 getSupportFragmentManager().beginTransaction()
                         .replace(R.id.bottom_nav_view, new TelaPartida()).commit();

                 return;
             case R.id.frag_principal_botao_personagem:
                 getSupportFragmentManager().beginTransaction()
                         .replace(R.id.bottom_nav_view, new TelaPersonagem()).commit();

                 return;
        }
     }
}
