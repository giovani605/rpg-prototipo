package com.example.rpg_prototipo.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.rpg_prototipo.R;

import java.util.ArrayList;

public class ListaHabilidadeAdapter extends ArrayAdapter {


    //to reference the Activity
    private Activity context;

    private ArrayList<ItemHabilidade> listaFeedItem;


    public ListaHabilidadeAdapter(Activity context1, ArrayList<ItemHabilidade> listaFeedItem) {
        super(context1, R.layout.fragment_frag_row_lista_inventario, listaFeedItem);
        this.context = context1;
        this.listaFeedItem = listaFeedItem;
    }
    // essa Funcao eh chamada para cada elemento da lista para colocar eles na tela
    // mapeia os item da lsita para a view
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.fragment_frag_row_lista_habilidades, null,true);

        //this code gets references to objects in the listview_row.xml file
        TextView descricao= (TextView) rowView.findViewById(R.id.frag_row_lista_habilidade_descricao);
        TextView dano = (TextView) rowView.findViewById(R.id.frag_row_lista_habilidade_dano);

        TextView ataque= (TextView) rowView.findViewById(R.id.frag_row_lista_habilidade_ataque);
        TextView nomeHabilidade = (TextView) rowView.findViewById(R.id.frag_row_habildiades_nome_habilidade);


        ItemHabilidade  item = listaFeedItem.get(position);

        //this code sets the values of the objects to values from the arrays
        descricao.setText(item.getDescricao());
        nomeHabilidade.setText(item.getNome());
        dano.setText(item.getDano());
        ataque.setText(item.getAtaque());


        return rowView;

    }




}
