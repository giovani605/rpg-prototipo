package com.example.rpg_prototipo.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.rpg_prototipo.R;

import java.util.ArrayList;

public class ListaInvetarioAdapter extends ArrayAdapter {


    //to reference the Activity
    private Activity context;

    private ArrayList<ItemInventario> listaFeedItem;


    public ListaInvetarioAdapter( Activity context1, ArrayList<ItemInventario> listaFeedItem) {
        super(context1, R.layout.fragment_frag_row_lista_inventario, listaFeedItem);
        this.context = context1;
        this.listaFeedItem = listaFeedItem;
    }
    // essa Funcao eh chamada para cada elemento da lista para colocar eles na tela
    // mapeia os item da lsita para a view
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.fragment_frag_row_lista_inventario, null,true);

        //this code gets references to objects in the listview_row.xml file
        TextView descricao= (TextView) rowView.findViewById(R.id.frag_row_lista_inventario_descricao);
        TextView nomeItem = (TextView) rowView.findViewById(R.id.frag_row_lista_inventario_nome_item);

        ItemInventario  item = listaFeedItem.get(position);

        //this code sets the values of the objects to values from the arrays
        descricao.setText(item.getDescricao());
        nomeItem.setText(item.getNome());


        return rowView;

    }




}
