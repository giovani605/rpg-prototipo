package com.example.rpg_prototipo.adapters;

public class ItemHabilidade {
    private String nome;
    private String descricao;
    private String dano;
    private String ataque;


    public ItemHabilidade(String nome, String descricao, String dano, String ataque) {
        this.nome = nome;
        this.descricao = descricao;
        this.dano = dano;
        this.ataque = ataque;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDano() {
        return dano;
    }

    public void setDano(String dano) {
        this.dano = dano;
    }

    public String getAtaque() {
        return ataque;
    }

    public void setAtaque(String ataque) {
        this.ataque = ataque;
    }
}
