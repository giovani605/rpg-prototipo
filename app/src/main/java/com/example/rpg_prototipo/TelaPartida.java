package com.example.rpg_prototipo;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TelaPartida#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TelaPartida extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public TelaPartida() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TelaPartida.
     */
    // TODO: Rename and change types and number of parameters
    public static TelaPartida newInstance(String param1, String param2) {
        TelaPartida fragment = new TelaPartida();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v =inflater.inflate(R.layout.fragment_tela_partida, container, false);
        Button b = (Button) v.findViewById(R.id.frag_partida_botao_dado);
        b.setOnClickListener(this);
        Random rand = new Random();
        TextView text = (TextView) v.findViewById(R.id.frag_partida_texto_dado);
        text.setText((rand.nextInt(19)+1)+"");
        this.getActivity().setTitle("Partida");
        return v;
    }

    @Override
    public void onClick(View v){
        Random rand = new Random();
        TextView text = (TextView) getView().findViewById(R.id.frag_partida_texto_dado);
        text.setText((rand.nextInt(19)+1)+"");
    }
}
