package com.example.rpg_prototipo;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TelaAtalhos#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TelaAtalhos extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public TelaAtalhos() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TelaAtalhos.
     */
    // TODO: Rename and change types and number of parameters
    public static TelaAtalhos newInstance(String param1, String param2) {
        TelaAtalhos fragment = new TelaAtalhos();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    private OnFragmentInteractionListener mListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }




    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void cliqueAtalho(View v);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onClick(View v){
        this.mListener.cliqueAtalho(v);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_tela_atalhos, container, false);

        Button button1 = (Button) v.findViewById(R.id.frag_atalhos_botao_Equipamento);
        button1.setOnClickListener(this);

        Button button2 = (Button) v.findViewById(R.id.frag_atalhos_botao_habilidades);
        button2.setOnClickListener(this);

        Button button3 = (Button) v.findViewById(R.id.frag_atalhos_botao_Inventario);
        button3.setOnClickListener(this);

        Button button4 = (Button) v.findViewById(R.id.frag_atalhos_botao_principal);
        button4.setOnClickListener(this);

        Button button5 = (Button) v.findViewById(R.id.frag_atalhos_botao_partida);
        button5.setOnClickListener(this);

        Button button6 = (Button) v.findViewById(R.id.frag_atalhos_botao_regras);
        button6.setOnClickListener(this);

        this.getActivity().setTitle("Atalhos");

        return v;
    }

}
