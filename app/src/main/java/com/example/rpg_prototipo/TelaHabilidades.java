package com.example.rpg_prototipo;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.example.rpg_prototipo.adapters.ItemHabilidade;
import com.example.rpg_prototipo.adapters.ItemRegra;
import com.example.rpg_prototipo.adapters.ListaHabilidadeAdapter;
import com.example.rpg_prototipo.adapters.ListaRegrasAdapter;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TelaHabilidades#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TelaHabilidades extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public TelaHabilidades() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TelaHabilidades.
     */
    // TODO: Rename and change types and number of parameters
    public static TelaHabilidades newInstance(String param1, String param2) {
        TelaHabilidades fragment = new TelaHabilidades();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_tela_habilidades, container, false);

        ArrayList<ItemHabilidade> listainventario = new ArrayList<>();
        ItemHabilidade itemInventario1 = new ItemHabilidade("Espada da Luz", "Sua Espada Brilha com a uma luz que queima os inimigos","SAB vs VON","1d8");
        listainventario.add(itemInventario1);
        ItemHabilidade itemInventario2 = new ItemHabilidade("Espada das Trevas", "DARKNESS","CAR vs REF","1d12");
        listainventario.add(itemInventario2);
        ItemHabilidade itemInventario3 = new ItemHabilidade("Disparo Magico", "Um tiro magico sai do seu cajado em direçao ao inimigo","INT vs REF","1d10" );
        listainventario.add(itemInventario3);

        ListAdapter adapter = new ListaHabilidadeAdapter(getActivity(),listainventario);

        ListView lista = (ListView) v.findViewById(R.id.frag_habilidades_lista_view);

        lista.setAdapter(adapter);

        this.getActivity().setTitle("Habilidades");

        return v;
    }

}
